# Autora:"Martha Cango"
# Email:"martha.cango@unl.edu.ec"
# Reescribe el programa que pide al usuario una lista de números e imprime el máximo y
# el mínimo de los números al final cuando el usuario ingresa “hecho”. Escribe el programa
# para almacenar los números que el usuario ingrese en una lista, y utiliza las funciones
# max() y min() para calcular el máximo y el mínimo después de que el bucle termine.
lista = []
while True:
    numero = input("Ingrese un número: ")
    lista.append(numero)
    if numero in "hecho":
        break
    maximo = max(lista)
    minimo = min(lista)

print("El número mayor es: ", maximo)
print("El número menor es: ", minimo)

