# Autora:"Martha Cango"
# Email:"martha.cango@unl.edu.ec"
# Escribir un programa para leer a través de datos de una bandeja de entrada de
# correo y cuando encuentres una línea que comience con “From”, dividir la línea
# en palabras utilizando la función split. Estamos interesados en quién envió el
# mensaje, lo cual es la segunda palabra en las líneas que comienzan con From.
usuario = open(input("Ingrese el nombre del archivo: "))
cont = 0
for linea in usuario:
    linea = linea.rstrip()
    if linea == "":
        continue
    palabra = linea.split()
    if palabra[0] != "From":
        continue
    print(palabra[1])
    cont = cont+1
print("Hay", cont, "lineas en el archivo con la palabra From al inicio")
